FROM alpine:3.9

RUN apk add --update --no-cache bash openssh python3

# install aws-cli
RUN pip3 install awscli --upgrade --user

# add aws-cli to path
RUN ln -s /root/.local/bin/aws /usr/local/bin

RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh

COPY pipe /

ENTRYPOINT ["/pipe.sh"]